<?PHP

namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class UserSeederCommand extends Command
{
    protected $db;
    protected $faker;

    public function __construct($db, $faker)
    {
        parent::__construct();
        $this->db = $db;
        $this->faker = $faker;
    }

    protected function configure()
    {
        $this->setName('seed:users')
             ->setDescription('Creates new users.')
             ->setHelp('This command allows you to create users...')
             ->addOption('count', 'c', InputOption::VALUE_REQUIRED, 'How many times do you want to input the repat', 1);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '          /––––––––––––––\\',
            '          | <options=bold>User Creator</> |',
            '          \––––––––––––––/',
            ''
        ]);

        $count = (int) $input->getOption('count');
        $userPlurilize = "user" . ($count < 2 ? '' : 's');

        $output->writeLn("<options=bold>[-]</> Add ${count} ${userPlurilize} in database !");

        $progressBar = new ProgressBar($output, $count);
        $progressBar->start();

        for($i=0; $i < $count; $i++) {
            $createUser = $this->db->prepare("
                INSERT INTO users (first_name, last_name, email)
                VALUES (:first_name, :last_name, :email)
            ");

            $createUser->execute(array(
                'first_name' => $this->faker->firstName,
                'last_name' => $this->faker->lastName,
                'email' => $this->faker->email
            ));

            $progressBar->advance();
        }

        $progressBar->finish();

        $output->writeLn("\r");

        $output->writeLn("<fg=green;options=bold>[v]</> <fg=green>${count} ${userPlurilize} created !</>");
    }
}
